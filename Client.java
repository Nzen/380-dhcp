import java.net.*;
import java.io.*;
import java.util.Random;
import java.util.Scanner;
import java.lang.StringBuilder;

// Usage
// command line > java Client port#

public class Client
{
	private static DataInputStream in;
	private static DataOutputStream out;
        private final static int ACKSIZE = 13;
        private final static int RELEASESIZE = 24;
	
	public static void main( String[] args ) throws IOException
	{
		testArgs( args.length );
		String server = "127.0.0.1";       // Server name/IP	
		int servPort = Integer.parseInt( args[0] );
		
		//Take input of client for random/select mac address
		String macaddress = getAddress( );
		System.out.println("\nUsing address " + macaddress);
		
		
		Socket serverConn = new Socket(server, servPort);
		System.out.println("\nConnecting to server at " + server + ", Port "  + servPort  );
                
		//expect the IP address
		
		handleSession( serverConn, macaddress );
		System.out.print("\nRequesting IP\t");
		serverConn.close();
	}
	
	static void testArgs( int numArgs ) throws IOException
	{
		if ( numArgs != 1 )
			throw new IllegalArgumentException( "\nRequired parameter(s): Port#" );
	}
	
	static String getAddress( )
	{
		String input;
		Scanner console = new Scanner(System.in);
		System.out.print( "\nTo supply an address, press 'y' -- " );
		input = console.nextLine( );
		if ( input.startsWith( "y" ) )
		{
			System.out.print( "Data Link Address -- " );
			input = console.nextLine( ).replace("\n","");
			return input;
		}
		else
			return generatedAddress( );
	}
	
	static String generatedAddress( ) // 6 hex pairs, colon separator
	{
		StringBuilder addr = new StringBuilder( );
		char[ ] hex = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		int lim = hex.length - 1;
		Random gen = new Random( 42 );
		for ( int nn = 6; nn > 0; nn-- )
		{
			addr.append( hex[ gen.nextInt( lim ) ] );
			addr.append( hex[ gen.nextInt( lim ) ] );
			addr.append( ':' );
		}
		addr.deleteCharAt( 17 );
		return addr.toString( );
	}
	
	static void handleSession( Socket serverConn , String MacAddress ) throws IOException
	{
		InputStream inPath = serverConn.getInputStream();
		in = new DataInputStream( inPath );
		OutputStream outPath = serverConn.getOutputStream();
		out = new DataOutputStream( outPath );

		byte[] data = MacAddress.getBytes();
		out.write(data);

		//wait for IP address
		String IPAddress;
		byte[] response = new byte[ACKSIZE];
		in.read( response );
		IPAddress = new String(response);

		System.out.print("\n" + IPAddress);

		//wait for IP release response
		response = new byte[RELEASESIZE];
		in.read( response );
		String release = new String(response);
		System.out.print("\n" + release);
	}
}
