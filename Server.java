import java.net.*;
import java.io.*;

// Usage
// command line > java Server 50000 (or OS appropriate ephemeral port)

//DHCP server
//1) The DHCP server manages IP addresses in the range 128.71.17.1 - 128.71.17.100
//2) The DHCP server will allocate the lowest numbered free IP address available, record the client's link layer address in its
// 	table, set a timer for a timeout of 5 seconds, send the IP address back to the client; the client will echo the
//	IP address back in its window.
//3) When the timer expires the server will remove the record from its table
//	and send the client a message back indicating that the lease has expired
//

public class Server
{
	private static final int maxClients = 100;
	public static void main( String[] args ) throws IOException
	{
		testArgs( args.length );
                
		int clients = 0;
		InputStream inPath;
		OutputStream outPath;
		Dhcp.initializeAddressTable();
		// use supplied server port
		int servPort = Integer.parseInt(args[0]);
                
		//printout of basis for DHCP server table
		System.out.print("IP Address\t\tClient\t\tTimer");
                
		try
		{
			ServerSocket servSock = new ServerSocket(servPort);
			Socket clntSock;
			while ( clients <= maxClients ) // if server overloaded, quit
			{
				clntSock = servSock.accept(); 
                                
				//Client connection established 
				inPath = clntSock.getInputStream();
				outPath = clntSock.getOutputStream();
				clients++;
				
                //passes the string to dhcp table class
				Dhcp newSession = new Dhcp( clntSock, inPath, outPath );
                                
				Thread core = new Thread( newSession );
				core.start();
				
				if ( core.getState() == Thread.State.TERMINATED ) // session over
					clients--;
			}
			servSock.close();
		}
		catch (IOException ioe)
		{
			System.out.println("IOException on socket listen: " + ioe);
			ioe.printStackTrace();
		}
     }
	 
	static void testArgs( int numArgs ) throws IOException
	{
		if ( numArgs != 1 )
			throw new IllegalArgumentException( "Only parameter: Port#" );
	}
}