import java.io.*;
import java.net.*;
import java.security.*;
import java.util.Hashtable;

	// -- confer with server, will you use a static table or use server's instance?

public class Dhcp implements Runnable
{
	private Socket connection;
	private DataInputStream in;
	private DataOutputStream out;
	private static Hashtable addressTable;
		
		private final static int MACSIZE = 20;
		
		public static void initializeAddressTable(){
			//Will be using hashtable, address table holds the ip addresses as keys and the client mac addresses as values
				addressTable = new Hashtable();
				for(int i = 0; i < 100; i++){
					if((i+1) < 10){
						addressTable.put((String)("129.71.17." + (i+1) + "  "), "n");
					}
					else if((i+1) < 100){
						addressTable.put((String)("129.71.17." + (i+1) + " "), "n");
					}
					else{
						addressTable.put((String)("129.71.17." + (i+1)), "n");
					}
					//stores ip address as key and mac address as indicator if occupied
					//values are intially set to blank string to inform of unoccupied ip address
				}
		}
		
	public Dhcp( Socket client, InputStream inPath, OutputStream outPath)
	{
		connection = client;
		in = new DataInputStream( inPath );
		out = new DataOutputStream( outPath );
	}

	@Override
	public void run()
	{
		handleSession( chooseLowestIP( ) );
	}
	
	// loops through addressTable's keys, looking for data that isn't the "n" flag
	private String chooseLowestIP( )
	{
		String ipAddress;
		int i = 1;
		// find an available ip address; ie whose data is "n"
		while(true){
			if(i < 10){
				if(addressTable.get((String)("129.71.17." + i + "  ")).equals("n")){
					ipAddress = "129.71.17." + i + "  ";
					break;
				}
			}
			else if(i < 100){
				if(addressTable.get((String)("129.71.17." + i + " ")).equals("n")){
					ipAddress = "129.71.17." + i + " ";
					break;
				}
			}
			else{
				if(addressTable.get((String)("129.71.17." + i)).equals("n")){
					ipAddress = "129.71.17." + i;
					break;
				}
			}
			if(i >= 100)
				i = 0; // loop again to find the lowest ip address available
			i++;
		}
		return ipAddress;
	}
	
	// listens for Client's Data Link Address; sends client IP
	private String assignIP( String ipAddress )
	{
		try
		{	// listen for mac address
			byte[] receiveMacAddress = new byte[MACSIZE];
			in.read( receiveMacAddress );
			
			String received = new String(receiveMacAddress);
			System.out.print("\n"+ ipAddress + "\t\t" + received + "\t\t" + "5");
			
			addressTable.put( ipAddress, received );
			// inform client of IP assigned
			byte[] ack = ipAddress.getBytes();
			out.write( ack );
			
			return received;
		}
		catch (IOException ioe)
		{
			System.out.println("IOException on socket listen: " + ioe);
			ioe.printStackTrace();
		}
		// assert unreachable
		return "failed";
	}
	
	// tells client IP; waits for expiration; closes connection
	private void handleSession( String ipAddress )
	{
		String mac = assignIP(ipAddress);
		waitForLeaseToExpire( ipAddress, mac );
		endSession(ipAddress);
		try
		{	connection.close();
		}
		catch (IOException ioe)
		{
			System.out.println("IOException on socket listen: " + ioe);
			ioe.printStackTrace();
		}
	}
	
	// sleep in one second intervals until lease expires
	private void waitForLeaseToExpire( String ipAddress, String mac )
	{
		for(int f = 5; f > 0; f--)
		{ 
			try{
				Thread.sleep(1000);//sleep for 5 seconds
				System.out.print("\n\n"+ ipAddress + "\t\t" + mac + "\t\t" + (f-1));
				// ## listen for an early release, or generate my own
			}
			catch( InterruptedException e)
			{
				Thread.yield( );  // let interrupting thread ahead
			}
		}
	}

	// inform client; put "n" flag in IP key of addressTable, signifying it is free
	private void endSession(String IpAddress)
	{
		try
		{
			String Mac = (String) addressTable.get(IpAddress);
			addressTable.put(IpAddress, "n"); //ip address is released and free to be used
			System.out.printf( "\tIP %s lease time expired.", IpAddress );
			// inform client
			out.write("IP lease time expired.".getBytes());
		}
		catch (IOException ioe)
		{
			System.out.println("IOException on socket listen: " + ioe);
			ioe.printStackTrace();
		}
	}
}
