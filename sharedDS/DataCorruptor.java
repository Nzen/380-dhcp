package sharedDS;

public class DataCorruptor
{

	public static void main(String[] args)
	{
		// init shared array
		Counters[ ] clientIPs = { new Counters(), new Counters(), new Counters(), new Counters() };
		WithSyncOnly nn = new WithSyncOnly( "1", clientIPs );
		WithSyncOnly uu = new WithSyncOnly( "2", clientIPs );
		WithSyncOnly cc = new WithSyncOnly( "3", clientIPs );
	}
}

class Counters // educational only so no get/set
{
	int left;
	public Counters( )
	{
		left = 0;
	}
}
/* Awesome run: threads switched which index to monitor
1.0	3 0 0 0 
3.2	3 3 3 0 
2.1	3 3 0 0 
1.0	2 2 2 0 
2.1	2 2 2 0 
3.2	2 2 2 0 
1.0	1 1 1 0 
3.2	1 1 1 0 
2.1	1 1 1 0 
3  next
1  next
3.0	3 0 0 0 
2  next
1.1	3 3 0 0 
2.2	2 3 3 0 
1.1	2 2 2 0 
3.0	2 2 2 0 
2.2	2 2 2 0 
1.1	1 1 1 0 
3.0	1 1 1 0 
2.2	1 1 1 0 
3  next
1  next
3.0	3 0 0 0 
2  next
1.1	3 3 0 0 
2.2	2 3 3 0 
1.1	2 2 2 0 
2.2	2 2 2 0 
3.0	2 2 2 0 
1.1	1 1 1 0 
2.2	1 1 1 0 
3.0	1 1 1 0 
2  next
1  next
3  next

*/