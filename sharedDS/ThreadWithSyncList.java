package sharedDS;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class ThreadWithSyncList implements Runnable
{
	final int SIZE = 5;
	protected static List< Boolean > sump = startSump();
	String threadName;
	Thread myself;
	
	// static initializer
	public static List<Boolean> startSump()
	{
		// fill with garbage so set( )s don't throw "out_of_bounds"
		ArrayList<Boolean> temp = new ArrayList<Boolean>( 6 ); // literal since I didn't want a static SIZE
		for ( int nn = 6; nn > 0; nn-- )
			temp.add( false );
		temp.set( 0, true );
		return Collections.synchronizedList( temp );
		// I've been warned that, hereafter, if temp is modified rather than sump,
		// it will defy synchronization, which is a danger to an instance based communal List.
	}
	
	public ThreadWithSyncList()
	{
		myself = new Thread( this );
		myself.start();
	}
	
	public ThreadWithSyncList( String name )
	{
		myself = new Thread( this );
		myself.start();
		this.threadName = name;
	}
	
	public void run()
	{
		int recover;
		try
		{
			recover = wreakHavoc();
			Thread.sleep( recover * 500 ); // to have them access at different times
			wreakHavoc();
			wreakHavoc();
			//removeHavoc();
			System.out.println( threadName + "-fin" );
		}
		catch ( InterruptedException wups )
		{ 
			//Thread.yield( ); // back of the thread queue for you
			System.out.print( "shoved\n" );
		}
	}
	
	// just a method to iterate and mutate values analagous to dhcp
	// should set correctly since I'm using the synchronized list
	int wreakHavoc()
	{
		/* find first with a true
		// if this is tail
			// set head to false
		// else
			// set next to true
		// print list */
		int ind = 0;
		Boolean flipped;
		synchronized( ThreadWithSyncList.sump ) // synchList demands manual sync for iteration
		{
			while ( ind <= SIZE )
			{
				flipped = ThreadWithSyncList.sump.get( ind ); // tedious: hazard of static field
				if ( flipped )
				{
					if ( ind == SIZE )
						// all are true
						ThreadWithSyncList.sump.set( 0, ! flipped );
					else // induct another
						ThreadWithSyncList.sump.set( ind + 1, flipped );
					break;
				}
				ind++;
			}
		}
		// result: X < 9 values are true, beginning from back
		//	except if head is true, then all true except tail.
		//printSump();
		return ( ind == SIZE ) ? SIZE : ind + 1;
	}
	
	synchronized void printSump()
	{
		for ( Boolean xY : ThreadWithSyncList.sump )
			System.out.print( threadName + ( ( xY ) ? "y " : "N " ) );
		System.out.println( );
	}
}

/* for better simulation later, tell me which thread changed it
class ynId
{
	boolean yes;
	int id;
	
	public ynId( )
	{
		yes = true;
		id = -1;
	}
	
	
}
*/

/*
-- Discarded, don't want to initialize five iterators.
Iterator<Boolean> syncSafeNow = ThreadWithArrList.sump.iterator();
// flip a true
while ( syncSafeNow.hasNext() )
{
	flipped = syncSafeNow.next();
	if ( flipped )
	{
		ThreadWithArrList.sump.set( ind, ! flipped );
		break;
	}
	ind++;
}
*/