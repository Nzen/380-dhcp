package sharedDS;

import java.lang.StringBuilder;

public class WithSyncOnly implements Runnable
{
	String threadName;
	Thread myself;
	int myIndex;
	Counters[ ] waits;
	
	public WithSyncOnly( String name,Counters[ ] all0 )
	{
		myIndex = -1;
		this.threadName = name;
		waits = all0;
		myself = new Thread( this );
		myself.start();
	}

	public void run()
	{
		int times = 3;
		try
		{
			while ( times > 0 ) // fake optimization: counting down
			{
				if ( choseCounter() )
				{
					while ( monitoringCounter() ) // side eff: counter--
						Thread.sleep( 500 );
					times--;
					System.out.print( threadName +"  next\n" );
				}
				else // all taken 
				{
					//System.out.print( "blocked from arr\n" );
					Thread.sleep( 250 );
					continue;
				}
			}
		}
		catch ( InterruptedException wups )
		{ 
			Thread.yield( ); // back of the thread queue for you
		}
	}
	
	boolean choseCounter()
	{
		int ind = 0;
		int lim = waits.length - 1;
		// find a counter to watch over
		synchronized( waits )
		{
			while ( ind <= lim )
			{
				if ( waits[ ind ].left < 1 )
				{
					myIndex = ind;
					break;
				}
				ind++;
			}
			// check that I got one
			if ( myIndex < 0 )
				return false;
			else
			{
				// set wait time
				waits[ myIndex ].left = 3;
				return true;
			}
		}
	}
	
	// decrement counter, return true if still above 0
	synchronized boolean monitoringCounter()
	{
		print();
		// decrement my counter
		waits[ myIndex ].left--;
		if ( waits[ myIndex ].left < 1 )
		{ // vigil ends
			myIndex = -1;
			return false;
		}
		return true; // more time to live
	}
	
	void print() // printing waits for I/O, so I save state with a string & print that
	{
		StringBuilder queue = new StringBuilder( 25 );
		queue.append( threadName + '.' + myIndex + "\t" );
		synchronized( waits )
		{
			for ( Counters dur : waits )
				queue.append( Integer.toString( dur.left ) + " " );
		}
		queue.append( "\n" );
		System.out.print( queue );
	}
}













